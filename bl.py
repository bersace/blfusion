import contextlib
import logging
import pdb
import sys
import xml.etree.ElementTree as ET
from textwrap import dedent


logger = logging.getLogger(__name__)


def auto_init(*, self, **attrs):
    self.__dict__.update(attrs)


class Item(object):
    id_alternatifs = {  # alternative -> référence BrickLink.
        "15672": "92946",
        "44237": "2456",
        "59443": "6538c",
        "60475": "30241",
        "94638": "87552",
        "98560": "3684c",
        "74695": "60483",
        "6143": "3941",
        "60475b": "30241b",
    }

    substitutions_id = {  # ancienne -> nouvelle
        # Drapeau corsaire bleu avec liseret ord.
        "2335""p04": "2335pb131",
        "2412""a": "2412b",
        "2555": "15712",
        # Cas particulier: milieu de coque ancien modèle avec attache brique
        # (2010) vs nouveau modèle avec attache fiche (2011).
        "2560": "95227",
        "2564": "95354",
        "3065": "3004",
        "3066": "3010",
        "3067": "3009",
        "3068""bpb0929": "3068bpb0161",
        "3794""a": "15573",
        "3794""b": "15573",
        "4085""a": "4085d",
        "4085""c": "4085d",
        "4150": "14769",
        "4460""a": "4460b",
        "4495": "4495b",
        "4495""a": "4495b",
        # Ancienne et nouvelle ancre de bateau
        "4589": "4589b",
        "4623": "88072",
        "4738""a": "4738b",
        "44302": "44302b",
        "47994": "19159",
        "53989": "98313",
    }

    substitutions_couleurs = {  # Couleur Mecabricks -> Couleur Bricklink
        # Metal argenté -> Flat Silver
        "67": "95",
        # Pearl-light-gray -> Flat Silver
        "66": "95",
    }

    @classmethod
    def depuis_xml(cls, el):
        id_ = el.find('./ITEMID')
        if id_ is None:
            return cls(
                type_='P',
                id_=el.find('MECABRICKSID').text,
                couleur=None,
                quantité=1,
            )
        else:
            el_couleur = el.find('./COLOR')
            return cls(
                type_=el.find('./ITEMTYPE').text,
                id_=id_.text,
                couleur=None if el_couleur is None else el_couleur.text,
                quantité=int(el.find('./MINQTY').text),
            )

    def __init__(self, type_, id_, couleur, quantité=0):
        auto_init(**locals())

    def copier(self, **kw):
        kw = dict(self.__dict__, **kw)
        return self.__class__(**kw)

    def corriger(self):
        reference = self.id_alternatifs.get(self.id_)
        if reference:
            logger.info(
                "Remplacement de l'ID alternatif %s par %s.",
                self.id_, reference)
            self.id_ = reference

        substitution = self.substitutions_id.get(self.id_)
        if substitution:
            logger.info(
                "Substitution de %s par %s.",
                self.id_, substitution,
            )
            self.id_ = substitution

        substitution_couleur = self.substitutions_couleurs.get(self.couleur)
        if substitution_couleur:
            logger.info(
                "Substitution de C%s par C%s.",
                self.couleur, substitution_couleur,
            )
            self.couleur = substitution_couleur
        return reference or substitution or substitution_couleur

    def __eq__(self, autre):
        return str(self.clef_tri()) == str(autre.clef_tri())

    def __hash__(self):
        return hash(str(self.clef_tri()))

    def __repr__(self):
        return '<%s %s>' % (self.__class__.__name__, self)

    def __str__(self):
        return '%s#%s C%s Q%s'  % (
            self.type_, self.id_, self.couleur, self.quantité,
        )

    def clef_tri(self):
        return (self.type_, self.couleur or "0", self.id_)


def afficher_liste(liste, fo):
    fo.write(dedent("""\
    <?xml version="1.0" encoding="UTF-8"?>
    <INVENTORY>
    """))
    for item in sorted(liste, key=Item.clef_tri):
        fo.write(dedent(f"""\
        <ITEM>
            <ITEMTYPE>{item.type_}</ITEMTYPE>
            <ITEMID>{item.id_}</ITEMID>
            <COLOR>{item.couleur}</COLOR>
            <MAXPRICE>-1.0000</MAXPRICE>
            <MINQTY>{item.quantité}</MINQTY>
            <CONDITION>X</CONDITION>
            <NOTIFY>N</NOTIFY>
        </ITEM>
        """))

    fo.write("""</INVENTORY>\n""")


def analyze_fichier(chemin):
    logger.info("Analyze de %s.", chemin)
    xml = ET.parse(chemin)
    for el in xml.findall('./ITEM'):
        try:
            item = Item.depuis_xml(el)
        except Exception as e:
            logger.error("Échec de l'analyze de %s: %s", repr_element_xml(el), e)
            continue
        yield item


def fusionner_listes(*listes):
    fusion = dict()
    for liste in listes:
        for item in liste:
            try:
                actuel = fusion[item]
            except KeyError:
                actuel = item.copier()
            else:
                logger.info("Fusion de %s.", actuel)
                actuel.quantité += item.quantité

            fusion[actuel] = actuel
    return fusion.values()


def repr_element_xml(el):
    r = [el.tag]
    r.extend(
        "%s->%s" % (enfant.tag, enfant.text)
        for enfant in el
    )
    return '<%s>' % ' '.join(r)


@contextlib.contextmanager
def exécution():
    logging.basicConfig(
        format="%(levelname)5.5s %(message)s",
        level=logging.DEBUG,
    )

    try:
        yield
    except pdb.bdb.BdbQuit:
        pass
    except Exception:
        logger.exception("Erreur inconnue:")
        if sys.stdout.isatty():
            pdb.post_mortem(sys.exc_info()[2])
        exit(1)
