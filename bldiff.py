#!/usr/bin/env python3
#
# Usage: ./bldiff.py possession.xml demande.xml > manquant.xml
#

import logging
import sys

import bl


logger = logging.getLogger('bldiff')


def programme(argv=sys.argv[1:]):
    possession = {i: i for i in bl.analyze_fichier(argv[0])}
    demande = bl.analyze_fichier(argv[1])
    manquant = []

    for item in demande:
        item_possédée = possession.get(item)
        if item_possédée is None:
            manquant.append(item)
            continue

        item_manquant = item.copier(
            quantité=item.quantité - item_possédée.quantité
        )

        if item_manquant.quantité > 0:
            manquant.append(item_manquant)

    bl.afficher_liste(manquant, sys.stdout)


if "__main__" == __name__:
    with bl.exécution():
        programme()
