# Fusionner des listes BrickLink

[BrickLink](https://bricklink.com) offre un outils pratique pour gérer l'achat
de pièces détachées : les *Wanted lists*. On peut notamment déplacer et copier
les items d'une liste vers une autre. À l'heure actuelle, on ne peut pas
additionner deux listes en unes. C'est là qu'intervient *blfusion*.

D'abord, télécharger les exports XML des listes à fusionner, puis lancer le script blfusion :

``` code
$ ./blfusion.py *.xml | xsel --clipboard --input
```

La fusion est dans votre presse-papier. Aller dans la [page d'envoi de
liste](https://www.bricklink.com/v2/wanted/upload.page), dans l'onget *Upload
BrickLink XML format*, copier votre presse-papier et suivez le formulaire.

Et voilà ! Les quantités sont additionnées et les couleurs sont préservées.
