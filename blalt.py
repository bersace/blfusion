#!/usr/bin/env python3
#
# Usage: ./bldiff.py possession.xml demande.xml > manquant.xml
#

import logging
import sys

import bl


logger = logging.getLogger('blalt')


def programme(argv=sys.argv[1:]):
    items = {i: i for i in bl.analyze_fichier(argv[0])}
    corrections = []

    # Corriger les items et les extraire.
    for item in items.copy().values():
        correction = item.copier()
        if correction.corriger():
            corrections.append(correction)
            del items[item]

    bl.afficher_liste(
        # Fusionner au cas où la version corrigé était déjà présente.
        bl.fusionner_listes(items.values(), corrections),
        sys.stdout,
    )


if "__main__" == __name__:
    with bl.exécution():
        programme()
