#!/usr/bin/env python3
#
# Usage: ./blfusion.py *.xml > fusion.xml
#

import logging
import sys

import bl

logger = logging.getLogger('blfusion')


def programme(argv=sys.argv[1:]):
    bl.afficher_liste(bl.fusionner_listes(*[
        bl.analyze_fichier(fichier)
        for fichier in argv
    ]), sys.stdout)


if "__main__" == __name__:
    with bl.exécution():
        programme()
